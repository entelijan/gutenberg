import Dependencies._

ThisBuild / scalaVersion := "3.1.2"
ThisBuild / version := "0.1.0-SNAPSHOT"
ThisBuild / organization := "entelijan"
ThisBuild / organizationName := "entelijan"

lazy val root = (project in file("."))
  .settings(
    name := "gutenberg",
    libraryDependencies += scalaTest % Test,
    libraryDependencies += "org.jsoup" % "jsoup" % "1.15.1",
    libraryDependencies += "commons-io" % "commons-io" % "2.11.0",
    libraryDependencies += "org.rogach" %% "scallop" % "4.1.0",
    libraryDependencies += "org.zeroturnaround" % "zt-zip" % "1.15",
    libraryDependencies += "org.slf4j" % "slf4j-simple" % "1.7.36",
    libraryDependencies += "org.w3c" % "epubcheck" % "4.2.6",
    libraryDependencies += "com.lihaoyi" %% "upickle" % "2.0.0",
  )

