package gutenberg

import org.jsoup.nodes.{Document, Element}

import scala.jdk.CollectionConverters.*

object Analyse {

  def metainfo(id: String, metadata: Metadata): Unit = {
    Metatype.values.flatMap { t =>
      metadata.content(t).map{v => (t, v)}
    }.foreach { tuple =>
      val l = "%25s %20s %-20s".format(id, tuple._1, tuple._2)
      println(l)
    }
 }
}