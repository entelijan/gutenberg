package gutenberg

import org.jsoup.nodes.Document

import java.nio.file.Path
import java.util.UUID.randomUUID

import upickle.default.{ReadWriter => RW, macroRW}

case class CommonValues(
                         htmlFileName: String,
                         navFileName: String,
                         tocFileName: String,
                         now: String,
                         title: String,
                         titleFileAs: String,
                         author: String,
                         pubId: String,
                         year: Option[String],
                         publisher: String,
                         firstPublished: String,
                         sender: String
                       )

object CommonValues {
  implicit val rw: RW[CommonValues ] = macroRW
}

case class CommonValuesIndexed(
                                index: String,
                                commonValues: CommonValues,
                              )

object CommonValuesIndexed {
  implicit val rw: RW[CommonValuesIndexed] = macroRW
}


object Commons {

  def create(bookId: String, md: Metadata): CommonValues = {

    def getPublisher: String = {
      if bookId == "dirnboec-mizi" then "Zeitschrift zur Förderung der Frauenbestrebungen"
      else {
        val p = md.content(Metatype.publisher).getOrElse("Herausgeber unbekannt")
        if p.length > 50 then throw IllegalArgumentException(s"$bookId. Publisher longer 50. '$p''")
        p
      }
    }

    def getFirstPublished: String = {
      def adaptYear(year: String): String = {
        try {
          val yearInt = year.toInt
          if yearInt > 1950 then "-"
          else year
        } catch {
          case _: Exception => "-"
        }
      }

      val py = md.content(Metatype.firstpub).getOrElse(md.content(Metatype.year).getOrElse("9999"))
      adaptYear(py)
    }


    val now = Utils.nowIso()
    val title = md.title
    val titleFileAs = Utils.fileAs(md.title)
    val pubId = md.content(Metatype.isbn).map(isbn => f"urn:isbn:$isbn").getOrElse(f"urn:uuid:$randomUUID")
    val year = md.content(Metatype.year)

    // Changes for specific books
    val author = if bookId == "dirnboec-mizi" then "Jenny Dirnböck-Schulz" else md.author

    CommonValues(
      htmlFileName = s"$bookId.xhtml",
      tocFileName = "toc.ncx",
      navFileName = "nav.xhtml",
      now = now,
      title = title,
      titleFileAs = titleFileAs,
      author = author,
      pubId = pubId,
      year = year,
      publisher = getPublisher,
      firstPublished = getFirstPublished,
      sender = md.content(Metatype.sender).getOrElse("-")
    )
  }

  def createMetadata(bookDir: Path, doc: Document): Metadata = {

    val nameCount = bookDir.getNameCount
    val defaultTitle = bookDir.getName(nameCount - 1).toString
    val defaultAuthor = bookDir.getName(nameCount - 2).toString
    Metadata(doc, defaultAuthor = defaultAuthor, defaultTitle = defaultTitle)
  }

}

