package gutenberg

import gutenberg.ImageFactory.CoverImageData
import gutenberg.Utils.CoverInfo
import org.apache.commons.io.FileUtils
import org.jsoup.Jsoup
import org.jsoup.nodes.*

import java.awt.datatransfer.MimeTypeParseException
import java.nio.charset.StandardCharsets
import java.nio.file.{Files, Path, Paths}
import java.util.UUID.{fromString, randomUUID}
import scala.io.Source
import scala.jdk.CollectionConverters.*


object EpubFactory {

  def processEpub(dataDir: Path, outDir: Path, analyse: Boolean): Unit = {

    val validResourceExtensions = List("jpg", "jpeg", "png", "svg")

    def findHtmlFile(bookDir: Path, stat: Stat): Option[Path] = {
      val bookDirName = bookDir.getFileName.toString
      val fileList = Utils.listFiles(bookDir)
      val htmlFilesFromBookDirName = fileList
        .filter { f =>
          f.getFileName.toString.equals(f"$bookDirName.html")
        }
      val htmlFiles = if htmlFilesFromBookDirName.nonEmpty then htmlFilesFromBookDirName
      else {
        fileList
          .filter { f =>
            f.getFileName.toString.toLowerCase().endsWith("html")
          }
      }
      if htmlFiles.isEmpty then {
        stat.addError(s"Found no html file for $bookDir")
        None
      }
      else {
        if htmlFiles.size > 1 then stat.addWarning(f"Found more than one html file in $bookDir")
        Some(htmlFiles.head)
      }
    }

    def processDataDir(commonsResDir: Path, stat: Stat, analyse: Boolean): Unit = {
      Files.list(dataDir).iterator.asScala
        .filter { p =>
          !p.getFileName.toString.equals("css") &&
            !p.getFileName.toString.equals("bin") &&
            !p.getFileName.toString.equals("info") &&
            !p.getFileName.toString.equals("autoren")
        }
        .foreach(authorDir => processAuthor(authorDir, commonsResDir, stat, analyse))
    }

    def processAuthor(authorDir: Path, commonsResDir: Path, stat: Stat, analyse: Boolean): Unit = {
      stat.incAuthCount()
      val authorOutDir = outDir.resolve(authorDir.getFileName)
      if Files.notExists(authorOutDir) then Files.createDirectories(authorOutDir)
      Files.list(authorDir).iterator.asScala
        .filter(Files.isDirectory(_))
        .foreach(processBook(_, commonsResDir, authorOutDir, stat, analyse))
    }

    def processResources(bookInDir: Path, commonsResDir: Path, bookOutDir: Path, bookContentDir: Path): Unit = {
      val resources = List(commonsResDir.resolve("css")) ++ Utils.listFiles(bookInDir)
      resources.foreach { resPath =>
        if Files.isDirectory(resPath) then {
          val fileName = resPath.getFileName.toString
          val outDir = bookContentDir.resolve(fileName)
          FileUtils.copyDirectory(resPath.toFile, outDir.toFile)
          println(s"Copied dir $resPath -> $outDir")
        }
        else {
          val fileName = resPath.getFileName.toString.toLowerCase()
          val fileExt = fileName.drop(fileName.lastIndexOf('.') + 1)
          if validResourceExtensions.contains(fileExt) then {
            FileUtils.copyFileToDirectory(resPath.toFile, bookContentDir.toFile)
            println(s"Copied file $resPath -> $bookOutDir")
          }
        }
      }
    }

    def processCover(bookId: String, doc: Document, commonValues: CommonValues, bookContentDir: Path) = {
      val coverImageData = CoverImageData(
        title = commonValues.title,
        author = commonValues.author,
        publisher = commonValues.publisher,
        year = commonValues.firstPublished,
      )

      val coversDir = outDir.resolve("covers")
      val imagesDir = bookContentDir.resolve("bilder")
      ImageFactory.createCover(bookId, coverImageData, coversDir, imagesDir)

      val coverInfo = Utils.coverImage(bookId, doc, false)
      val coverFileName = coverInfo.fileName
      val coverPath = bookContentDir.resolve(coverFileName)
      val coverContent = createCover(commonValues, coverInfo.imageUrl)
      Files.write(coverPath, coverContent.getBytes(StandardCharsets.UTF_8))
      println(s"Wrote cover to $coverPath")
      coverInfo
    }

    def processBook(bookInDir: Path, commonsResDir: Path, authorOutDir: Path, stat: Stat, analyse: Boolean): Unit = {
      val bookId = Utils.getBookId(bookInDir)
      try {
        println("--------------------------------------------------------")
        println(f"Input dir $bookInDir")

        val bookDirName = bookInDir.getFileName.toString
        val bookOutDir = authorOutDir.resolve(bookDirName)
        if Files.notExists(bookOutDir) then Files.createDirectories(bookOutDir)

        findHtmlFile(bookInDir, stat).foreach { htmlFile =>
          val doc: Document = Jsoup.parse(htmlFile.toFile)
          val metadata = Commons.createMetadata(bookInDir, doc)
          val commonValues = Commons.create(bookId, metadata)

          val adaptedDoc = adaptHtml(bookId, doc)

          if analyse then {
            Analyse.metainfo(bookId, metadata = metadata)
          }
          else {
            val bookContentDir = bookOutDir.resolve("OEBPS")
            if Files.notExists(bookContentDir) then Files.createDirectories(bookContentDir)

            val metaInfDir = bookOutDir.resolve("META-INF")
            if Files.notExists(metaInfDir) then Files.createDirectories(metaInfDir)

            processResources(bookInDir, commonsResDir, bookOutDir, bookContentDir)
            val coverInfo: CoverInfo = processCover(bookId, doc, commonValues, bookContentDir)

            val mimetypePath = bookOutDir.resolve(s"mimetype")
            val mimeTypeContent = "application/epub+zip"
            Files.write(mimetypePath, mimeTypeContent.getBytes(StandardCharsets.US_ASCII))
            println(s"Wrote mimetype to $mimetypePath")

            val containerPath = metaInfDir.resolve(s"container.xml")
            val containerXml = createContainerXml
            Files.write(containerPath, containerXml.getBytes(StandardCharsets.UTF_8))
            println(s"Wrote container xml to $containerPath")

            val cleanHtmlPath = bookContentDir.resolve(commonValues.htmlFileName)
            val cleanHtmlContent = createHtml(adaptedDoc)
            Files.write(cleanHtmlPath, cleanHtmlContent.getBytes(StandardCharsets.UTF_8))
            println(s"Wrote clean html to $cleanHtmlPath")

            val navPath = bookContentDir.resolve(commonValues.navFileName)
            val navContent = createNav(commonValues)
            Files.write(navPath, navContent.getBytes(StandardCharsets.UTF_8))
            println(s"Wrote navigation (nav) to $navPath")

            val tocPath = bookContentDir.resolve(commonValues.tocFileName)
            val tocContent = createToc(commonValues)
            Files.write(tocPath, tocContent.getBytes(StandardCharsets.UTF_8))
            println(s"Wrote table of content (toc) to $tocPath")

            val opfPath = bookContentDir.resolve("package.opf")
            val opfContent = createOpf(commonValues, commonsResDir, bookContentDir, coverInfo)
            Files.write(opfPath, opfContent.getBytes(StandardCharsets.UTF_8))
            println(s"Wrote opf to $opfPath")

            val nc = bookOutDir.getNameCount
            val name = s"entelijan-gutenberg-${bookOutDir.getName(nc - 2)}-${bookOutDir.getName(nc - 1)}.epub"
            val allOutDir = outDir.resolve("epub")
            if Files.notExists(allOutDir) then Files.createDirectories(allOutDir)
            val epubFile = allOutDir.resolve(name)
            Utils.compressEpub(bookOutDir, epubFile)
            Utils.validateEpub(epubFile, stat)
            println(s"Created epub as $epubFile")
            stat.addData(commonValues)
            stat.incBookCount()
          }
        }
      }
      catch {
        case e:Exception =>
          stat.addError(s"Error processing $bookId. ${e.getMessage}")
      }
    }

    def adaptHtml(bookId: String, doc: Document): Document = {

      // remove doctype
      doc.root().childNodes().forEach { node =>
        if node.isInstanceOf[DocumentType] then node.remove()
      }

      // add xhtml declaration
      val decl = new XmlDeclaration("xml", false)
      decl.attr("version", "1.0")
      decl.attr("encoding", "UTF-8")
      doc.root().prepend(decl.toString)

      // Add epub name spaces
      doc.select("html").attr("xmlns", "http://www.w3.org/1999/xhtml")
      doc.select("html").attr("xmlns:epub", "http://www.idpf.org/2007/ops")
      doc.select("html").attr("xml:lang", "de")

      doc.head().select("script").remove()

      doc.head().select("meta[charset]").attr("charset", "utf-8")

      doc.head().select("*[href]").forEach { witHref =>
        val attrValue = witHref.attr("href")
        if attrValue.startsWith("../../") then witHref.attr("href", attrValue.drop(6))
      }

      // adapt body
      doc.body().select(".navi-gb-ed15").remove()

      doc.body().select("tt").forEach { node =>
        val t = node.text()
        node.replaceWith(doc.createElement("span").text(t))
      }

      doc.body().select("big").forEach { node =>
        val t = node.text()
        node.replaceWith(doc.createElement("span").text(t))
      }

      doc.body().select("img[width]").forEach { node =>
        if !Utils.isInt(node.attr("width")) then node.removeAttr("width")
      }

      doc.body().select("img[vspace]").forEach { node =>
        node.removeAttr("vspace")
      }

      doc.body().select("img[hspace]").forEach { node =>
        node.removeAttr("hspace")
      }

      doc.body().select("hr[size]").forEach { node =>
        node.removeAttr("size")
      }

      doc.body().select("hr[width]").forEach { node =>
        node.removeAttr("width")
      }

      doc.body().select("table[cellpadding]").forEach { node =>
        node.removeAttr("cellpadding")
      }

      doc.body().select("table[align]").forEach { node =>
        node.removeAttr("align")
      }

      doc.body().select("tr[valign]").forEach { node =>
        node.removeAttr("valign")
      }

      doc.body().select("td[nowrap]").forEach { node =>
        node.removeAttr("nowrap")
      }

      doc.body().select("div[type]").forEach { node =>
        node.removeAttr("type")
      }

      doc.body().select("br[clear]").forEach { node =>
        node.removeAttr("clear")
      }

      doc.body().select("table[width]").forEach { node =>
        node.removeAttr("width")
      }

      doc.body().select("table[cellspacing]").forEach { node =>
        node.removeAttr("cellspacing")
      }

      doc.body().select("table[summary]").forEach { node =>
        node.removeAttr("summary")
      }

      doc.body().select("td[align]").forEach { node =>
        node.removeAttr("align")
      }

      doc.body().select("tr[align]").forEach { node =>
        node.removeAttr("align")
      }

      doc.body().select("td[valign]").forEach { node =>
        node.removeAttr("valign")
      }

      doc.body.select("a[name=_GoBack]").remove()

      // Changes for specific books
      if bookId == "dose-muttsoh1" then {
        doc.body().select("a[href]").forEach { node =>
          if node.attr("href").contains("pfaffen.html") then {
            val t = node.text()
            node.replaceWith(doc.createElement("span").text(t))
          }
        }
      }
      doc
    }

    def createHtml(doc: Document): String = {
      // output xhtml
      doc.outputSettings()
        .syntax(Document.OutputSettings.Syntax.xml)
        .escapeMode(Entities.EscapeMode.xhtml)
      doc.html()
    }

    def createContainerXml: String = {
      """<?xml version="1.0"?>
        |<container version="1.0" xmlns="urn:oasis:names:tc:opendocument:xmlns:container">
        |<rootfiles>
        |<rootfile full-path="OEBPS/package.opf" media-type="application/oebps-package+xml"/>
        |</rootfiles>
        |</container>
        |""".stripMargin
    }

    def createNav(commonValues: CommonValues): String = {
      s"""<?xml version='1.0' encoding='utf-8'?>
         |<html xmlns="http://www.w3.org/1999/xhtml" xmlns:epub="http://www.idpf.org/2007/ops">
         |<head>
         |<title>Navigation</title>
         |</head>
         |<body>
         |<nav epub:type="toc">
         |<h2>Inhaltsverzeichnis</h2>
         |<ol epub:type="list">
         |<li><a href="${commonValues.htmlFileName}">${commonValues.author}, ${commonValues.title}</a></li>
         |</ol>
         |</nav>
         |</body>
         |</html>
         |""".stripMargin
    }

    def createToc(commonValues: CommonValues): String = {
      s"""<?xml version='1.0' encoding='utf-8'?>
         |<ncx xmlns="http://www.daisy.org/z3986/2005/ncx/" version="2005-1" xml:lang="de">
         |<head>
         |<meta name="dtb:uid" content="${commonValues.pubId}"/>
         |</head>
         |<docTitle>
         |<text>${commonValues.title}</text>
         |</docTitle>
         |<navMap>
         |<navPoint class="other" id="navpoint-1" playOrder="1">
         |  <navLabel>
         |    <text>${commonValues.author}, ${commonValues.title}</text>
         |  </navLabel>
         |  <content src="${commonValues.htmlFileName}"/>
         |</navPoint>
         |</navMap>
         |</ncx>
         |""".stripMargin
    }

    def createCover(commonValues: CommonValues, imageUrl: String): String = {
      s"""<?xml version="1.0"?>
         |<html lang="en-US" xmlns="http://www.w3.org/1999/xhtml" xmlns:epub="http://www.idpf.org/2007/ops">
         |<head>
         |<title>${commonValues.title}</title>
         |<meta http-equiv="default-style" content="text/html; charset=utf-8"/>
         |</head>
         |<body style="margin-top: 0px; margin-left: 0px; margin-right: 0px; margin-bottom: 0px; text-align: center; background-color:#00007;">
         |<div class="cover">
         |<img src="$imageUrl" alt="cover"/>
         |</div>
         |</body>
         |</html>""".stripMargin
    }

    def createOpf(commonValues: CommonValues, commonResDir: Path, bookDir: Path, coverInfo: CoverInfo): String = {
      val styleFiles = {
        val fMimeType = (p: Path) =>
          if p.getFileName.toString.toLowerCase().endsWith("css")
          then Some("text/css")
          else None
        Utils.resourceElements(commonResDir, "css", fMimeType).map { r =>
          s"""<item id="${r.id}" href="${r.reference}" media-type="${r.mimeType}"/>"""
        }.mkString("\n")
      }

      val fMimeType = (p: Path) => Utils.imageMimeType(p)
      val imageResources = Utils.resourceElements(bookDir, "image", fMimeType)

      def findImageId(imageResource: String): String = {
        val ir = imageResources.filter(r => r.reference == imageResource)
        if ir.isEmpty then {
          val resStr = imageResources.map(_.reference).mkString(",")
          throw IllegalStateException(s"ERROR: Could not find '$imageResource' in '$resStr'")
        }
        ir.head.id
      }

      val imageFiles = {
        imageResources.map { r =>
          s"""<item id="${r.id}" href="${r.reference}" media-type="${r.mimeType}"/>"""
        }.mkString("\n")
      }
      val yearElem = commonValues.year.map(y => f"""<meta property="dcterms:date">$y</meta>""").getOrElse("")
      val coverFileNameItem = f"""<item id="${coverInfo.id}" href="${coverInfo.fileName}" media-type="application/xhtml+xml"/>"""

      val coverItemRef = f"""<itemref idref="${coverInfo.id}"/>"""
      val coverImageMeta = {
        val imageId = findImageId(coverInfo.imageUrl)
        f"""<meta name="${coverInfo.id}" content="$imageId"/>"""
      }
      f"""<?xml version="1.0" encoding="UTF-8"?>
         |<package xmlns="http://www.idpf.org/2007/opf" version="3.0" xml:lang="de" unique-identifier="pub-id"
         |  prefix="rendition: http://www.idpf.org/vocab/rendition/#">
         |<metadata xmlns:dc="http://purl.org/dc/elements/1.1/">
         |<meta refines="#title" property="file-as">${commonValues.titleFileAs}</meta>
         |<meta property="dcterms:modified">${commonValues.now}</meta>
         |<dc:title id="title">${commonValues.title}</dc:title>
         |<dc:identifier id="pub-id">${commonValues.pubId}</dc:identifier>
         |<dc:language>de</dc:language>
         |<dc:creator id="creator">${commonValues.author}</dc:creator>
         |<dc:publisher>projekt-gutenberg.de</dc:publisher>
         |<dc:rights>CC BY</dc:rights>
         |<meta property="dcterms:creator" id="auth">${commonValues.author}</meta>
         |$yearElem
         |<meta property="rendition:orientation">auto</meta>
         |<meta property="rendition:spread">auto</meta>
         |$coverImageMeta
         |</metadata>
         |<manifest>
         |<item id="contents" href="${commonValues.htmlFileName}" media-type="application/xhtml+xml"/>
         |<item id="toc" href="${commonValues.tocFileName}" media-type="application/x-dtbncx+xml"/>
         |<item id="nav" href="${commonValues.navFileName}" media-type="application/xhtml+xml" properties="nav"/>
         |$coverFileNameItem
         |$styleFiles
         |$imageFiles
         |</manifest>
         |<spine page-progression-direction="ltr" toc="toc">
         |<itemref idref="contents"/>
         |$coverItemRef
         |</spine>
         |</package>
         |""".stripMargin
    }


    val stat = Stat()
    val commonResDir = Files.createTempDirectory("gutenberg")
    val resources = List(
      "css/author.css",
      "css/drama.css",
      "css/info.css",
      "css/jugendst.css",
      "css/poem.css",
      "css/prosa.css",
    )
    Utils.copyResourcesToDirectory(commonResDir, resources)
    processDataDir(commonResDir, stat, analyse)
    val statFile = outDir.resolve("stat.txt")
    FileUtils.writeStringToFile(statFile.toFile, stat.format, "UTF-8")

    val dataList = upickle.default.write(stat.datas)
    val dataFile = outDir.resolve("gutenberg.json")
    FileUtils.writeStringToFile(dataFile.toFile, dataList, "UTF-8")
    println(s"Wrote data to ${dataFile.toAbsolutePath}")

    println(stat.format)
    println(s"Wrote statistics to $statFile")
  }


}
