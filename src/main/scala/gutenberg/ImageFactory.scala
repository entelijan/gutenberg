package gutenberg

import org.apache.commons.io.FileUtils

import scala.sys.process.*
import java.nio.file.{Files, Path}

object ImageFactory {

  case class CoverImageData(
                             title: String,
                             author: String,
                             publisher: String,
                             year: String,
                           )

  def createCover(id: String, coverImageData: CoverImageData, workDir: Path, imagesDir: Path): String = {
    if Files.notExists(workDir) then Files.createDirectories(workDir)
    val p2 = imagesDir.resolve("images/p2.png")
    if Files.notExists(p2) then Utils.copyResourcesToDirectory(imagesDir, Seq("images/p2.png"))
    val backgroundImage = Path.of("data/covers/p2.png")
    FileUtils.copyFileToDirectory(backgroundImage.toFile, workDir.toFile)
    val coverHtmlFile = workDir.resolve(s"cover-$id.html")
    val htmlContent = createHtml(coverImageData)
    FileUtils.writeStringToFile(coverHtmlFile.toFile, htmlContent, "UTF-8")
    println(s"Wrote cover html to '$coverHtmlFile'")

    if Files.notExists(imagesDir) then Files.createDirectories(imagesDir)
    val coverFileName = s"cover-$id.png"
    val coverFile = imagesDir.resolve(coverFileName)
    val cmd = s"""chromium --headless --disable-gpu --window-size=1200,1600 --screenshot=$coverFile $coverHtmlFile"""
    val code = cmd.!
    if code != 0 then throw RuntimeException(s"Error creating cover image for $id using chromium")
    FileUtils.copyFileToDirectory(coverFile.toFile, workDir.toFile)
    println(s"Wrote cover to '${coverFile.toAbsolutePath}'")
    s"bilder/$coverFileName"
  }

  private def createHtml(coverImageData: CoverImageData): String = {
     s"""<!DOCTYPE html>
        |<html lang="en">
        |<head>
        |<meta charset="UTF-8">
        |    <title>Mein lieblings Titel</title>
        |</head>
        |<body style="margin:0; font-family: times, serif;">
        |    <div style="width:1200px; height:1600px; background-color: rgb(188, 218, 142); background-image: url('p2.png'); background-repeat: repeat; background-size: auto;">
        |        <div style="position: absolute; top: 50px; left:50px; height:1500px; width: 1100px; background-color: white;  border: black solid 1px; opacity: 0.8;">
        |            <div style="position: absolute; top: 100px; left:50px; width: 1000px; background-color: white; opacity: 1.0;">
        |                <div style="margin-top: 20px; text-align: center; font-size: 130px;">${coverImageData.title}</div>
        |                <div style="margin-top: 60px; text-align: center; font-size: 90px;">${coverImageData.author}</div>
        |                <div style="margin-top: 60px; text-align: center; font-size: 90px;">${coverImageData.year}</div>
        |            </div>
        |            <div style="position: absolute; top: 1200px; left:50px; width: 1000px; background-color: white; opacity: 1.0; ">
        |                <div style="margin-top: 10px; text-align: center; font-size: 60px;">${coverImageData.publisher}</div>
        |            </div>    
        |        </div>    
        |    </div>
        |</body>
        |</html>
      """.stripMargin
  }
}
