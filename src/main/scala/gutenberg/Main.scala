package gutenberg

import org.apache.commons.io.FileUtils
import org.rogach.scallop._

import java.nio.file.{Files, Paths}

//noinspection TypeAnnotation
object Main {

  def main(args: Array[String]): Unit = {
    val conf = new Conf(args)
    conf.subcommand match {
      case Some(conf.epub) =>
        println("Started command epub")
        val dataDir = Paths.get(conf.epub.dataDir())
        val outDir = Paths.get(conf.epub.outDir())
        if Files.notExists(outDir) then Files.createDirectories(outDir)
        FileUtils.deleteDirectory(outDir.toFile)
        println(f"Deleted $outDir")
        EpubFactory.processEpub(dataDir, outDir, false)
      case Some(conf.analyse) =>
        println("Started command analyse")
        val dataDir = Paths.get(conf.analyse.dataDir())
        val outDir = Paths.get(conf.analyse.outDir())
        EpubFactory.processEpub(dataDir, outDir, true)
      case Some(conf.tryout) =>
        println("Started command tryout")
        Tryout.run()
      case _ =>
        throw IllegalStateException(s"Unknown subcommand ${conf.subcommand}")
    }
  }

  private class Conf(arguments: Seq[String]) extends ScallopConf(arguments) {
    object epub extends Subcommand("epub") {
      val dataDir = opt[String](required = true, descr = "Input directory containing authors/books")
      val outDir = opt[String](required = true, descr = "Output directory")
    }

    object analyse extends Subcommand("analyse") {
      val dataDir = opt[String](required = true, descr = "Input directory containing authors/books")
      val outDir = opt[String](required = true, descr = "Output directory")
    }

    object tryout extends Subcommand("tryout") {}

    addSubcommand(epub)
    addSubcommand(analyse)
    addSubcommand(tryout)
    verify()
  }

}

