package gutenberg

import org.jsoup.*
import org.jsoup.nodes.Document
import org.jsoup.select.Elements

import scala.jdk.CollectionConverters.*

enum Metatype: 
    case    title,
            author,
            sender,
            publisher,
            created,
            `type`,
            year,
            corrector,
            projectid,
            firstpub,
            booktitle,
            address,
            pages,
            translator,
            pfad,
            isbn,
            printrun,
            editor,
            volume,
            series,
            modified,
            secondcorrector,
            illustrator,
            wgs,
            note

class Metadata(doc: Document, defaultAuthor: String, defaultTitle: String) {

    private val data: Map[String, String] = doc.select("meta[name]").asScala
        .map{ e =>
            val k = e.attr("name")
            val v = e.attr("content")
            (k, v)
        }.toMap

    def content(key: Metatype): Option[String] = data.get(key.toString)
    def title: String = content(Metatype.title).getOrElse(defaultTitle)
    def author: String = content(Metatype.author).getOrElse(defaultAuthor)

}
