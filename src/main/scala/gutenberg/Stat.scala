package gutenberg

import scala.collection.mutable

class Stat {
  var authCount = 0
  var bookCount = 0
  var errors = List.empty[String]
  var warnings = List.empty[String]
  var datas = List.empty[CommonValues]
  var keys = List.empty[String]

  def incAuthCount(): Unit = authCount += 1

  def incBookCount(): Unit = bookCount += 1

  def addError(error: String): Unit = this.errors = error :: errors

  def addWarning(warning: String): Unit = this.warnings = warning :: warnings

  def addKey(key: String): Unit = this.keys = key :: keys

  def addData(line: CommonValues): Unit = this.datas  = line :: datas

  def format: String = {
    val sb = mutable.StringBuilder()
    sb.append("\n--------------------------------------------------\n")
    val fstring = "%15s | %10d\n"
    val fstringInt = "%15s | %s\n"
    sb.append(fstring.format("author count", this.authCount))
    sb.append(fstring.format("book count", this.bookCount))
    if this.errors.isEmpty then sb.append(fstringInt.format("errors", "-"))
    else this.errors.zipWithIndex.foreach { (msg, i) =>
      if i == 0 then sb.append(fstringInt.format("errors", msg))
      else sb.append(fstringInt.format("", msg))
    }
    if this.warnings.isEmpty then sb.append(fstringInt.format("warnings", "-"))
    else this.warnings.zipWithIndex.foreach { (msg, i) =>
      if i == 0 then sb.append(fstringInt.format("warnings", msg))
      else sb.append(fstringInt.format("", msg))
    }
    sb.toString()
  }

  def formatShort: String = {
    val sb = mutable.StringBuilder()
    sb.append("\n--------------------------------------------------\n")
    val fstring = "%15s | %10d\n"
    val fstringInt = "%15s | %s\n"
    sb.append(fstring.format("author count", this.authCount))
    sb.append(fstring.format("book count", this.bookCount))
    sb.append(fstring.format("error count", this.errors.size))
    sb.append(fstring.format("warning count", this.warnings.size))
    sb.toString()
  }
}
