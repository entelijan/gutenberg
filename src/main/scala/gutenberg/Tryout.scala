package gutenberg

import org.apache.commons.io.FileUtils
import upickle.*

import java.nio.file.{Files, Paths}
import java.text.Normalizer

object Tryout {

  def createIndex(commonValues: CommonValues): CommonValuesIndexed = {
    val base = commonValues.author + commonValues.title + commonValues.publisher
    val index = Normalizer
      .normalize(base, Normalizer.Form.NFD)
      .replaceAll("[\\p{InCombiningDiacriticalMarks}]", "")
      .toLowerCase
      .replaceAll("[^a-zA-Z0-9]+", "")
    CommonValuesIndexed(index, commonValues)
  }


  def run(): Unit = {
    println("tryout")
    println("reading data")
    val infile = Paths.get("data", "small-gutenberg.json")
    println(s"reading ${infile.getFileName}")
    if Files.notExists(infile) then throw IllegalStateException(s"${infile.toAbsolutePath} does not exits")
    println(s"${infile.toAbsolutePath} exists")
    val values = upickle.default.read[List[CommonValues]](infile.toFile)
    val cvi  = values.map(createIndex)
    val json = upickle.default.write(cvi, 2)
    val outfile = Paths.get("data", "small-gutenberg-index.json")
    FileUtils.writeStringToFile(outfile.toFile, json, "UTF-8")
    println(s"Wrote indexed values to $outfile")

  }

}
