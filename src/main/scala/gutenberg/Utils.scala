package gutenberg

import org.apache.commons.io.FileUtils
import org.jsoup.nodes.{Document, Element}
import org.jsoup.select.Elements

import java.io.{BufferedReader, File, FileOutputStream, Reader, StringWriter}
import java.nio.file.{Files, Path, Paths, StandardCopyOption}
import scala.io.{BufferedSource, Source}
import org.zeroturnaround.zip.{NameMapper, ZipUtil}

import scala.jdk.CollectionConverters.*
import sys.process.*
import java.time.Instant
import java.time.format.DateTimeFormatter
import java.time.temporal.TemporalAccessor
import scala.::
import scala.annotation.tailrec

object Utils {

  case class ElementReference(
                               id: String,
                               reference: String,
                               mimeType: String,
                             )

  def resourceElements(baseDir: Path, prefix: String, fMimeType: Path => Option[String]): Iterable[ElementReference] = {
    Files.walk(baseDir)
      .toList.asScala
      .filter(p => Files.isRegularFile(p))
      .flatMap(p => fMimeType(p).map(mt => (mt, p)))
      .zipWithIndex
      .map { tuple =>
        val path = tuple._1._2
        val mimeType: String = tuple._1._1
        val n1 = baseDir.getNameCount
        val n2 = path.getNameCount
        val reference = path.subpath(n1, n2).toString
        val index = tuple._2
        val indexStr = "%03d".format(index)
        ElementReference(s"$prefix$indexStr", reference, mimeType)
      }
  }

  private val noFileWords = List(
    "der",
    "die",
    "das",
    "ein",
    "eine",
    "den",
    "dem",
  )

  def fileAs(text: String): String = {

    def convert(): String = {
      val textSplit = text.split("\\s")
      if textSplit.size == 1 then text
      else {
        val tailReverse = textSplit.tail.reverse
        val withComma = tailReverse.head.appended(',') :: tailReverse.tail.toList
        val words = withComma.reverse ++ Seq(textSplit.head)
        words.mkString(" ")
      }
    }

    @tailrec
    def _fileAs(_noFileWords: List[String]): String = {
      _noFileWords match {
        case Nil => text
        case noFileWord :: rest =>
          if text.toLowerCase.startsWith(noFileWord) then convert()
          else _fileAs(rest)
      }
    }

    if text == null then null
    else _fileAs(noFileWords)
  }

  def compressEpub(baseDir: Path, zipPath: Path): Unit = {
    val names = Files.list(baseDir)
      .toList
      .asScala
      .map { p =>
        val nam = p.getFileName.toString
        if nam.toLowerCase.startsWith("mime") then (0, nam)
        else if nam.toLowerCase.startsWith("meta") then (5, nam)
        else (10, nam)
      }
      .sortBy(_._1)
      .map(_._2)
    val zipFile = zipPath.toAbsolutePath.toString
    val cmd = Seq("zip", "-rqX", zipFile) ++ names
    val p = Process(cmd, baseDir.toFile).!
    if p != 0 then throw RuntimeException(s"Could not zip $baseDir to $zipFile")
    else println(s"Zipped $baseDir to $zipFile")
  }

  private val imageExtensions = List(
    "jpg",
    "jpeg",
    "tiff",
    "png",
    "gif",
  )

  def imageMimeType(path: Path): Option[String] = mediaType(path.getFileName.toString)

  def mediaType(coverInfo: CoverInfo): String = mediaType(coverInfo.imageUrl).get

  private def mediaType(nam: String): Option[String] = {
    val i = nam.lastIndexOf('.')
    val ext = nam.substring(i + 1).toLowerCase()
    if imageExtensions.contains(ext) then {
      if "jpg".equals(ext) then Some("image/jpeg")
      else Some(s"image/$ext")
    }
    else None
  }

  def nowIso(): String = {
    val timeFormatter = DateTimeFormatter.ofPattern("YYYY-MM-dd'T'HH:mm:ss'Z'")
    timeFormatter.format(java.time.LocalDateTime.now())
  }

  def listFiles(dir: Path): List[Path] = {
    Files.list(dir)
      .iterator
      .asScala
      .toList
  }

  def copyResourcesToDirectory(dir: Path, resources: Iterable[String]): Unit = {
    resources.foreach { resName =>
      val is = this.getClass.getClassLoader.getResourceAsStream(resName)
      val file = dir.resolve(Paths.get(resName))
      println(f"Copy $resName -> $file")
      FileUtils.copyToFile(is, file.toFile)
    }
  }

  import java.io.PrintWriter
  import com.adobe.epubcheck.api.*
  import com.adobe.epubcheck.util.*

  def validateEpub(epub: Path, stat: Stat): Unit = {
    val sw = StringWriter()
    val pw = PrintWriter(sw)
    val r = WriterReportImpl(pw, epub.toAbsolutePath.toString, true)
    val validator = EpubCheck(epub.toFile, r)
    val validationCode = validator.doValidate()
    if validationCode != 0 then {
      println(s"ERROR in epub validation for $epub")
      val message = sw.getBuffer.toString
      stat.addError(message)
    }
    else println(s"Epub validation OK for $epub")
  }

  def isInt(string: String): Boolean = {
    try {
      Integer.parseInt(string)
      true
    } catch {
      case _: Exception => false
    }
  }

  case class CoverInfo(
                        id: String,
                        imageUrl: String,
                        fileName: String,
                      )

  def coverImage(bookId: String, doc: Document, withExistingCover: Boolean): CoverInfo = {

    def idTitlepage: Option[CoverInfo] = {
      doc
        .body().select("div[id=titlepage]")
        .asScala.toList.flatMap(_.select("img").asScala.toList)
        .headOption.map { el =>
        CoverInfo(
          imageUrl = el.attr("src"),
          fileName = "cover.xhtml",
          id = "cover",
        )
      }
    }

    def created: CoverInfo = {
      CoverInfo(
        imageUrl = s"bilder/cover-$bookId.png",
        fileName = "cover.xhtml",
        id = "cover",
      )
    }

    if withExistingCover then {
      if bookId == "dominik-ewigherz" then created
      else idTitlepage.getOrElse(created)
    }
    else created
  }

  def getBookId(bookDir: Path): String = {
    val author = bookDir.getParent.getFileName.toString
    val title = bookDir.getFileName.toString
    s"$author-$title"
  }
}
