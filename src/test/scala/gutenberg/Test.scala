package gutenberg

import gutenberg.Utils.CoverInfo
import org.jsoup.Jsoup
import org.scalatest.funsuite.AnyFunSuite

import java.nio.file.{Files, Path}
import javax.swing.plaf.metal.MetalComboBoxEditor

class Test extends AnyFunSuite {

  List(
    ("a.jpg", Some("image/jpeg")),
    ("a.JPG", Some("image/jpeg")),
    ("a.png", Some("image/png")),
    ("a.tiff", Some("image/tiff")),
    ("a.TIFF", Some("image/tiff")),
    ("a.gif", Some("image/gif")),
    ("a.jpeg", Some("image/jpeg")),
    ("hallo/a.jpeg", Some("image/jpeg")),
    ("hallo/a.txt", None),
    ("a.txt", None),
    ("a", None),
    ("hallo/", None),
    ("hallo/hallo", None),
    ("hallo/.hallo", None),
  ).foreach { (name, expected) =>
    test(s"is image $name") {
      val p = Path.of(name)
      assert(Utils.imageMimeType(p) == expected)
    }
  }

  List(
    ("Hungerlohn", "Hungerlohn"),
    ("Der Hungerlohn", "Hungerlohn, Der"),
    ("Ein Mann", "Mann, Ein"),
    ("Ein", "Ein"),
    ("Dem Kind ist nichts zu viel", "Kind ist nichts zu viel, Dem"),
    ("", ""),
    (null, null),
  ).foreach { (title, expected) =>
    test(s"file as $title") {
      val as = Utils.fileAs(title)
      assert(as == expected)
    }

  }

}
