epub1
-    fix errors from complete run
-    ../../autoren/signatur resource
-    split chapters.  create toc for chapters
-    fill series element
-    add some tags. On top gutenberg genre ???
-    fix errors from complete run
-    Dumas zwei mal als Author Ä und Ä.
-    Der Graf von Montechristo erster band Titelbild fehlt
-    Calibre: Verlag besser den urspr. Verlag und nicht gutenberg.org
-    Calibre: Datum besser das Ersterscheinung und nicht epub erzeugung
-    Warum sind beim Import in calibre duplikate. Auch bei den tesbüchern 'a'. Der Brand der Cheops... 
-    Genre als metadaten. poem, novel, ...
-    Viele Authoren sind mit unterschiedlichen Namen erfasst. Finde Duplikate automatisiert
-    Buch 'Der gestiefelte Kater' ist leer. Warum ?
-    Author b8cd5fc8
-    Die Kreuzfahrer Felix Dahn. Titel Author vertauscht
-    'Lieder aus China' Bilder fehlen
-    'Der Mann in Salz' Titel Author vertauscht
-    Convert equal authors to one
-    Convert equal publisher to one
-    Remove Author from title if it was wrongly added
-    Cover from last to first page
-    Cover too big for tolino
-    take 'Wolfsblut' as an example. Works perfect on tolino.

adjust html
- OK remove navi
- OK set charset to utf8
- OK remove the script tag
- OK copy css
- OK make css based on out dir no ../..
- OK copy images if they exist
- OK make image link based on out dir. no ../..

epub
- OK print ß in /diez/leben Fu'ß'note
- OK title sort string
- OK language en -> de
- OK version 2.0 3.0. Took 3.0 because has more usable tags.
- OK check urnID OK ? Is OK
- OK toc should show chapter or at least title not 'toc'
- OK existing title image if exists else create
- OK adapt authors e.g "A B (1998 - 2000)"
- OK find cover for body/div/div/img[src=~cover.*] e.g. dose-muttersoh1

